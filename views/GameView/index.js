const Computer = require('../../models/RandomComputer');
const chalk = require('chalk');

/**
 * Handles the presentation of the state of the game to the console
 *
 * 'presenting' a view means clearing the view, and then presenting new information
 * 'drawing' means just getting a string to be presented
 */
class GameView {
    static presentSplashScreen() {
        console.clear();
        console.log(
            `We're playing...                                                                                 
                                           ,--.     ,---.                       
,---. ,---. ,--,--, ,--,--,  ,---.  ,---.,-'  '-.  /  .-' ,---. ,--.,--.,--.--. 
| .--'| .-. ||      \\|      \\| .-. :| .--''-.  .-'  |  \`-,| .-. ||  ||  ||  .--' 
\\ \`--.' '-' '|  ||  ||  ||  |\\   --.\\ \`--.  |  |.--.|  .-'' '-' ''  ''  '|  |    
 \`---' \`---' \`--''--'\`--''--' \`----' \`---'  \`--''--'\`--'   \`---'  \`----' \`--'`
        );
    }

    /**
     * Cleans the terminal and presents the current board state
     *
     * @param {Board} board - The board of the game we want to present
     */
    static presentCurrentBoardState(board) {
        console.clear();
        console.log('Current Board state...');
        console.log(this.drawBoard(board));
    }

    /**
     * Cleans the terminal and presents the ending win/lose state
     *
     * @param {Board} board - The board of the game we want to present
     * @param {Player} winner - The winner of the game
     */
    static presentEndState(board, winner) {
        console.clear();
        console.log('We have a winner!!!');
        console.log(this.drawBoard(board, true));

        if (winner instanceof Computer) {
            console.log(`Oh no... the Computer won! Try again next time :'(`);
        } else {
            console.log(`Congratulations Player ${winner.symbol} - You've WON!!!`);
        }
    }

    /**
     *Cleans the terminal and presents a state where no one has won
     *
     * @param {Board} board - The board of the game we want to present
     */
    static presentDrawState(board) {
        console.clear();
        console.log('Ladies and Gentlemen... it appears we have a draw...?!');
        console.log(this.drawBoard(board));
        console.log(`Congratulations to both players, you've fought well!`);
    }

    /**
     * Draws the given board as ASCII art, without presenting the board
     *
     * @param {Board} board - The board to be drawn
     * @param {boolean} [shouldColorWinningRow=false] - Whether or not we should color the winning row
     * @return {string}
     */
    static drawBoard(board, shouldColorWinningRow = false) {
        let drawing = [];

        // We're drawing this board starting from the rows because it's easier to interpolate the strings later on by row, rather than by column
        let rowLength = board.columns[0].length;

        // For each row, we're adding a new array of columns in that row, so we can join them later
        while(drawing.length < rowLength) {
            drawing.push([]);
        }

        board.columns.forEach((column, columnIndex) => {
            // Reverse the rows, as the 0th row should be on the bottom, not the top
            let reversedRows = column.slice().reverse();

            reversedRows.forEach((row, index) => {
                let symbol = row ? this.drawPiece(row) : '_';

                // If we should colour pieces, and this piece is part of a winning group, then color it green
                if (shouldColorWinningRow && board.isConnectedByFour(columnIndex, column.indexOf(row))) {
                    symbol = chalk.green(symbol);
                }

                drawing[index] = drawing[index].concat(['|', symbol]);
            });
        });

        drawing.forEach((row, index) => {
            // Cap off the drawing
            row.push('|');

            // Join the row's strings so that we can a clean row with all of the necessary columns
            drawing[index] = row.join(' ');
        });

        // Add numbering at the bottom of each column
        let footer = [];
        board.columns.forEach((column, index) => {
            footer.push(`  ${board.isFillableColumn(index) ? index + 1 : ' '} `);
        });
        drawing.push(footer.join(''));

        return drawing.join('\n') + '\n';
    }

    /**
     * Draws the given piece - but does not present it
     *
     * @param {Piece} piece - The piece that should be drawn
     * @return {string}
     */
    static drawPiece(piece) {
        return piece.player.symbol;
    }
}

module.exports = GameView;