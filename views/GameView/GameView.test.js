const GameView = require('./');
const Player = require('../../models/Player');
const Piece = require('../../models/Piece');
const Board = require('../../models/Board');

// Suppress console logs as they're only for presentation purposes
process.stdout.write = jest.fn();
console.log = jest.fn();

describe('GameView', () => {
    test('can draw a board', () => {
        let board = new Board();

        let boardState =
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '  1   2   3   4   5   6   7 \n';
        expect(GameView.drawBoard(board)).toBe(boardState);

        let player = new Player('X');
        let piece = new Piece(player);
        board.addPiece(piece, 0);
        boardState =
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| _ | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '  1   2   3   4   5   6   7 \n';
        expect(GameView.drawBoard(board)).toBe(boardState);

        let piece2 = new Piece(player);
        let piece3 = new Piece(player);
        let piece4 = new Piece(player);
        let piece5 = new Piece(player);
        let piece6 = new Piece(player);
        board.addPiece(piece2, 0);
        board.addPiece(piece3, 0);
        board.addPiece(piece4, 0);
        board.addPiece(piece5, 0);
        board.addPiece(piece6, 0);
        boardState =
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '      2   3   4   5   6   7 \n';
        expect(GameView.drawBoard(board)).toBe(boardState);

        let piece7 = new Piece(player);
        board.addPiece(piece7, 3);
        boardState =
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | _ | _ | _ | _ |\n' +
            '| X | _ | _ | X | _ | _ | _ |\n' +
            '      2   3   4   5   6   7 \n';
        expect(GameView.drawBoard(board)).toBe(boardState);
    });

    test('can draw a piece with the correct symbol', () => {
        let player = new Player('X');
        let piece = new Piece(player);
        expect(GameView.drawPiece(piece)).toBe('X');

        let player2 = new Player('O');
        let piece2 = new Piece(player2);
        expect(GameView.drawPiece(piece2)).toBe('O');
    });
});