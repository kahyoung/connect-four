const Board = require('../../models/Board');
const Piece = require('../../models/Piece');
const GameView = require('../../views/GameView');

/**
 * The controller of the actual Game - combines the Board and Players together for them to actually play a game
 *
 * Business logic for the game exists here
 *
 * @constructor
 * @params {Player[]} - The players of the game
 */
class GameController {
    constructor(players) {
        this._board = new Board();
        this._players = players;
        this._moveCount = 0;

    }

    /**
     * Plays the next turn for the next player, recursively
     *
     * The winner of the game is returned in the final promise
     *
     * @return {Promise.<Player>}
     */
    async play() {
        GameView.presentCurrentBoardState(this._board);

        let currentPlayer = this._players[this._moveCount % 2];
        let index = await currentPlayer.makeMove(this._board.fillableColumns);

        while(!this._board.isFillableColumn(index)) {
            console.log(`Hey, you can't do that! It isn't a valid column!!!`);

            index = await currentPlayer.makeMove(this._board.fillableColumns);
        }

        this._board.addPiece(new Piece(currentPlayer), index);

        this._moveCount++;

        if (this._board.isConnectedByFour(index)) {
            // The winner is the person who's turn it was last
            let winner = this._players[(this._moveCount - 1) % 2];
            GameView.presentEndState(this._board, winner);

            return winner;
        } else if (this._board.fillableColumns.length === 0) {
            GameView.presentDrawState(this._board);
            return null;
        } else {
            return this.play();
        }
    }
}

module.exports = GameController;