const GameController = require('./');
const Player = require('../../models/Player');
const Computer = require('../../models/RandomComputer');
const stdin = require('bdd-stdin');

// Suppress console logs as they're only for presentation purposes
process.stdout.write = jest.fn();
console.log = jest.fn();

describe('GameController', () => {
   test('can be constructed without failing', () => {
      new GameController([new Player('O'), new Player('X')]);
   });

   test('can continuously prompt players for their turn', () => {
       let game = new GameController([new Player('O'), new Player('X')]);
       stdin('1', '2', '1', '2', '1', '2', '1');

       return game.play();
   });

   test('can be played with a Computer players', () => {
       let game = new GameController([new Computer('O'), new Computer('X')]);
       return game.play();
   });

   // Occurred from a bug when determining if the game was over - game would crash
    test('can have columns filled with alternating pieces', () => {
        let game = new GameController([new Player('0'), new Player('X')]);
        stdin(
            '1', '1', '1', '1', '1', '1',
            '2', '2', '2', '2', '2', '2',
            '3', '3', '3', '3', '3', '3',
            '7', '7', '7', '7', '7', '7',
            '6', '6', '6', '6', '6', '6',
            '5', '5', '5', '5', '5', '5',
            '4'
        );

        return game.play();
    });

   test('can have rows filled with alternating pieces', () => {
       let game = new GameController([new Player('0'), new Player('X')]);
       stdin(
           '1', '2', '3', '4', '5', '6', '7',
           '1', '2', '3', '4', '5', '6', '7',
           '1', '2', '3', '4', '5', '6', '7',
           '1', '2', '3', '4', '5', '6', '7'
       );

       return game.play();
   });

   test('can determine a vertical winner', () => {
       let playerO = new Player('O');
       let playerX = new Player('X');
       let game = new GameController([playerO, playerX]);

       stdin('1', '2', '1', '2', '1', '2', '1');

       return game.play().then(winner => {
           expect(winner).toBe(playerO);
       });
   });

    test('can determine a horizontal winner', () => {
        let playerO = new Player('O');
        let playerX = new Player('X');
        let game = new GameController([playerO, playerX]);

        stdin('1', '1', '2', '2', '3', '3', '4');

        return game.play().then(winner => {
            expect(winner).toBe(playerO);
        });
    });

    test('can determine a descending diagonal winner', () => {
        let playerO = new Player('O');
        let playerX = new Player('X');
        let game = new GameController([playerO, playerX]);

        stdin('1', '1', '1', '1', '1', '2', '2', '2', '3', '3', '3', '4');

        return game.play().then(winner => {
            expect(winner).toBe(playerX);
        });
    });

    test('can determine an ascending diagonal winner', () => {
        let playerO = new Player('O');
        let playerX = new Player('X');
        let game = new GameController([playerO, playerX]);

        stdin('1', '2', '2', '3', '3', '2', '3', '2', '4', '4', '5', '4', '4');

        return game.play().then(winner => {
            expect(winner).toBe(playerO);
        });
    });

    test('can determine when there are no winners', () => {
        let game = new GameController([new Player('O'), new Player('X')]);
        stdin(
            '1', '1', '1', '1', '1', '1',
            '2', '2', '2', '2', '2', '2',
            '3', '3', '3', '3', '3', '3',
            '7', '7', '7', '7', '7', '7',
            '6', '6', '6', '6', '6', '6',
            '5', '5', '5', '5', '5', '4',
            '4', '4', '4', '4', '4', '5'
        );

        return game.play().then(winner => {
            expect(winner).toBe(null);
        });
    });
});