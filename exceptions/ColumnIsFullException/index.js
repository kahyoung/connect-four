function ColumnIsFullException() {
    this.name = 'ColumnIsFullException';
    this.message = 'The column that you attempted to add a piece to is full already!';
}

module.exports = ColumnIsFullException;