const GameController = require('./controllers/GameController');
const Player = require('./models/Player');
const Computer = require('./models/RandomComputer');
const GameView = require('./views/GameView');

GameView.presentSplashScreen();

setTimeout(() => {
    let game = new GameController([new Player('O'), new Computer('X')]);
    game.play().then(() => process.exit());
}, 1000);