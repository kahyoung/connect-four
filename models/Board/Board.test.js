const Board = require('./');
const Piece = require('../Piece');
const Player = require('../Player');
const ColumnIsFullException = require('../../exceptions/ColumnIsFullException');

// Suppress console logs as they're only for presentation purposes
process.stdout.write = jest.fn();
console.log = jest.fn();

describe('Board', () => {
    test('can be constructed without failing', () => {
        new Board();
    });

    test('can be constructed with default values of 7 columns and 6 rows', () => {
        let board = new Board();

        expect(board.columns.length).toBe(7);
        expect(board.columns[0].length).toBe(6);
    });

    test('can be constructed with specified columns and rows', () => {
        let board = new Board(2, 4);

        expect(board.columns.length).toBe(2);
        expect(board.columns[0].length).toBe(4);
    });

    test('can determine if a given column index is fillable', () => {
       let board = new Board();

       expect(board.isFillableColumn(0)).toBe(true);
    });

    test('can get the indices of columns that are yet to be filled', () => {
        let board = new Board();

        expect(board.fillableColumns).toEqual([0, 1, 2, 3, 4, 5, 6]);
    });

    test('can have a piece added to it', () => {
        let board = new Board();
        let playerO = new Player('O');
        let playerX = new Player('X');
        let piece = new Piece(playerO);

        board.addPiece(piece, 0);
        expect(board.columns[0][0]).toBe(piece);

        // Fill the column up with alternating pieces
        board.addPiece(new Piece(playerX), 0);
        board.addPiece(new Piece(playerO), 0);
        board.addPiece(new Piece(playerX), 0);
        board.addPiece(new Piece(playerO), 0);
        board.addPiece(new Piece(playerX), 0);
        expect(board.columns[0].includes(null)).toBe(false);

        // Check other columns
        expect(board.columns[1].every(row => row === null)).toBe(true);
    });

    test('will throw a ColumnIsFullException when adding a piece to a filled column', () => {
       let board = new Board();
       let player = new Player();
       let piece = new Piece(player);
       let piece2 = new Piece(player);
       let piece3 = new Piece(player);
       let piece4 = new Piece(player);
       let piece5 = new Piece(player);
       let piece6 = new Piece(player);
       let piece7 = new Piece(player);
       board.addPiece(piece, 0);
       board.addPiece(piece2, 0);
       board.addPiece(piece3, 0);
       board.addPiece(piece4, 0);
       board.addPiece(piece5, 0);
       board.addPiece(piece6, 0);

       expect(() => board.addPiece(piece7, 0).toThrow(ColumnIsFullException));
    });
});