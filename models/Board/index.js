const ColumnIsFullException = require('../../exceptions/ColumnIsFullException');

/**
 * A representation of a Connect Four board
 *
 * The Board is the container of pieces, where players can place pieces in any particular column
 *
 * NOTE: The columns property is a 2-dimensional array
 *
 * The 1st dimension is each column, from left to right. e.g.:
 * | _ | _ | _ | _ | _ | _ | _ |
 *   0   1   2   3   4   5   6;
 *
 * The 2nd dimension is each row of each column, from  bottom to top. e.g.:
 * 5 | _ |
 * 4 | _ |
 * 3 | _ |
 * 2 | _ |
 * 1 | _ |
 * 0 | _ |
 *
 * @constructor
 * @params {number=6} columnLength - The amount of columns that this board will have
 * @params {number=7} rowLength - The amount of rows that this board will have
 */
class Board {
    constructor(columnLength = 7, rowLength = 6) {
        this.columns = [];

        while(this.columns.length < columnLength) {
            let column = [];

            while(column.length < rowLength) {
                column.push(null);
            }

            this.columns.push(column);
        }
    }

    /**
     * Determines if the column at the given index can have any
     * @param {number} index - The index that we want to check
     * @return {boolean}
     */
    isFillableColumn(index) {
        let column = this.columns[index];

        return !!column && column.includes(null);
    }

    /**
     * Returns an array which contains the indices of columns that haven't been completely filled with pieces yet
     *
     * @return {number[]}
     */
    get fillableColumns() {
        return this.columns.reduce((accumulator, column, index) => {
            if (this.isFillableColumn(index)) {
                accumulator.push(index);
            }

            return accumulator;
        }, []);
    }

    /**
     * Adds the given piece to the column at the given index
     *
     * If the column that the piece should be added to is already full, then it throws a ColumnIsFullException
     * NOTE: The board does not handle this exception - it's up to the game itself to handle it
     *
     * @param piece
     * @param columnIndex
     * @throws ColumnIsFullException
     */
    addPiece(piece, columnIndex) {
        if (this.isFillableColumn(columnIndex)) {
            let column = this.columns[columnIndex];
            let firstEmptyRow = column.indexOf(null);

            column[firstEmptyRow] = piece;
        } else {
            throw new ColumnIsFullException();
        }
    }

    /**
     * Checks the last piece at the given columnIndex to see if it is connected by four to any neighbouring piece
     *
     * It is connected by four if it is a part of a chain of 4 pieces that are the same vertically, horizontally, or diagonally
     *
     * @params {number} columnIndex - The column to check
     */
    isConnectedByFour(columnIndex, rowIndex) {
        let column = this.columns[columnIndex];
        // We want to find the LAST not-null index, so we find the FIRST null index, and get the piece behind that
        if (rowIndex == null) {
            rowIndex = column.findIndex(piece => piece === null) - 1;
        }

        // If we can't find any null piece, it means the column is full, then we use the last row index
        if (rowIndex < 0) {
            rowIndex = column.length - 1;
        }

        let piece = this.columns[columnIndex][rowIndex];

        // If there's no piece in the index, then it can't be connected
        if (piece == null) {
            return false;
        }

        /**
         * Checks to see if there are any sequential groups vertically
         *
         * @return {boolean}
         */
        let checkVertically = () => {
            return checkForSequentialLimit(column, piece);
        };

        /**
         * Checks to see if there are any sequential groups horizontally
         *
         * @return {boolean}
         */
        let checkHorizontally = () => {
            // Grab all the pieces in the same row
            let row = this.columns.reduce((accumulator, column) => accumulator.concat(column[rowIndex]), []);

            return checkForSequentialLimit(row, piece);
        };

        /**
         * Checks to see if there are any sequential groups diagonally
         *
         * Diagonal checks can be both descending and ascending, where a descending diagonal looks like:
         * X
         *  X
         *   X
         *    X
         *
         * And a ascending diagonal looks like
         *      X
         *     X
         *    X
         *   X
         * @params {boolean} [isAscending=true] - Whether or not we should be checking an ascending diagonal or not
         */
        let checkDiagonally = (isAscending = false) => {
            let descendingCoordinates = [columnIndex, rowIndex];
            let diagonalDirection = isAscending ? -1 : 1;

            // To find the origin of the diagonal, we need to go back a column, and either go higher, or lower in rows depending on the direction
            while (descendingCoordinates[0] > 0 && descendingCoordinates[1] < column.length && descendingCoordinates[1] >= 0) {
                descendingCoordinates[0]--;
                descendingCoordinates[1] += diagonalDirection;
            }

            let diagonalPieces = [];

            // Now that we know where this diagonal starts, we can start adding all the diagonal pieces
            while (descendingCoordinates[0] < this.columns.length && descendingCoordinates[1] < column.length && descendingCoordinates[1] >= 0) {
                diagonalPieces.push(this.columns[descendingCoordinates[0]][descendingCoordinates[1]]);
                descendingCoordinates[0]++;
                descendingCoordinates[1] -= diagonalDirection;
            }

            return checkForSequentialLimit(diagonalPieces, piece);
        };

        return checkVertically() || checkHorizontally() || checkDiagonally() || checkDiagonally(true);

        /**
         * Given a list of pieces, this method will group the pieces up into sequential groups (i.e. only pieces that
         * are one after another) and then check to see if any of the groups are as long as the limit
         *
         * Returns true if there are any sequential groups that are at least 4 pieces long
         *
         * @param {Piece[]} pieces - The list of pieces to check
         * @param {Piece} piece - The piece to match the other pieces with - all the pieces need to be the same
         * @param {number} [limit = 4] - The amount of sequential pieces needed in order to be considered a win
         * @return {boolean}
         */
        function checkForSequentialLimit(pieces, piece, limit = 4) {
            // We want to get all the sequential pieces together in their own arrays so that later we can just check to see if there are any sequential groups that have 4 or more pieces
            let sequentialGroups = pieces.reduce((accumulator, columnPiece, index) => {
                if (piece.isOwnedBySamePlayer(columnPiece)) {
                    let group = accumulator.find(group => group[group.length - 1] + 1 === index);

                    if (group) {
                        group.push(index);
                    } else {
                        group = [index];
                        accumulator.push(group);
                    }
                }

                return accumulator;
            }, []);

            return sequentialGroups.some(sequentialPieces => sequentialPieces.length >= limit);
        }
    }
}

module.exports = Board;