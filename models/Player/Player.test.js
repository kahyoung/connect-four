const Player = require('./');
const stdin = require('bdd-stdin');

// Suppress console logs as they're only for presentation purposes
process.stdout.write = jest.fn();
console.log = jest.fn();

describe('Player', () => {
    test('can be constructed without failing', () => {
        new Player('X');
    });

    test('can be prompted to make a move', () => {
        let player = new Player('X');

        stdin('1');
        return player.makeMove([0, 1, 2, 3, 4, 5, 6, 7])
            .then(index => {
                // The returned INDEX is 0 because the user selected the 1ST column
                expect(index).toBe(0);
            });
    });
});