/**
 * A representation of a Player
 *
 * A player is the acting body of the game - they use Pieces and place them on the Board to play the game
 *
 * @params {string} symbol - The symbol of this player, used to differentiate players in the game
 */
class Player {
    constructor(symbol) {
        this.symbol = symbol;
    }

    /**
     * Allows a player to make a choice and select a column in which they want to place their piece
     *
     * NOTE: It does not place a piece, just returns a number that can be used in the Game to place the piece
     *
     * @param {number[]} availableIndices - A list of column indexes which are valid for the player to select. Starting from 0
     * @return {Promise.<number>}
     */
    makeMove(availableIndices) {
        let availableColumnNumbers = availableIndices.map(index => index + 1);
        console.log(`Column${availableIndices.length > 1 ? 's' : ''} ${availableColumnNumbers.join(', ')} can be filled`);

        return new Promise((resolve) => {
            process.stdout.write(`Player ${this.symbol} - Which column do you want to drop your piece in? `);
            process.stdin.once('data', (answer) => {
                resolve(answer - 1);
            });
        });
    }
}

module.exports = Player;