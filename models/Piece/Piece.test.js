const Piece = require('./');
const Player = require('../Player');

// Suppress console logs as they're only for presentation purposes
process.stdout.write = jest.fn();
console.log = jest.fn();

describe('Piece', () => {
    test('can be constructed without failing', () => {
        let player = new Player('X');
        new Piece(player);
    });
});