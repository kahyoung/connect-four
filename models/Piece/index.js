/**
 * A representation of a Connect Four piece
 *
 * A piece is owned by a player, and is a specific colour - it is placed within the Board
 *
 * @constructor
 * @params {Player} player - The player that owns this piece
 */
class Piece {
    constructor(player) {
        this.player = player;
    }

    /**
     * Checks to see if the given piece is owned by the same player as this piece
     *
     * @param {Piece} piece - The piece to compare
     * @return {boolean}
     */
    isOwnedBySamePlayer(piece) {
        return !!piece && this.player === piece.player;
    }
}

module.exports = Piece;