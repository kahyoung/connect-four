const Player = require('../Player');

/**
 * A representation of a Computer that plays with a Random strategy. It is an extension of the {@link Player} class
 *
 * @params {string} symbol - The symbol of this player, used to differentiate players in the game
 */
class RandomComputer extends Player {

    /**
     * Allows a player to make a choice and select a column in which they want to place their piece
     *
     * NOTE: It does not place a piece, just returns a number that can be used in the Game to place the piece
     *
     * @param {number[]} availableIndices - A list of column indexes which are valid for the player to select. Starting from 0
     * @return {Promise.<number>}
     */
    makeMove(availableIndices) {
        return new Promise(resolve => {
            console.log('Computer is thinking...');

            setTimeout(() => {
                let index = Math.floor(Math.random() * availableIndices.length);
                resolve(availableIndices[index]);
            }, 100);
        });
    }
}

module.exports = RandomComputer;