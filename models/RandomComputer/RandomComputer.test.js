const Computer = require('./');
const Player = require('../Player');

// Suppress console logs as they're only for presentation purposes
process.stdout.write = jest.fn();
console.log = jest.fn();

describe('RandomComputer', () => {
    test('can be constructed without failing', () => {
        new Computer('X');
    });

    test('is an instance of a Player', () => {
        expect(new Computer('X')).toBeInstanceOf(Player);
    });

    test('can be prompted to make a move', () => {
        let computer = new Computer('X');

        let availableIndices = [0, 1, 2, 3, 4, 5, 6, 7];
        return computer.makeMove(availableIndices)
            .then(index => {
                expect(availableIndices).toContain(index);
            });
    });
});