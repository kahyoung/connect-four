# Connect Four

This is a simple Connect Four game that you can play in your terminal - written in JavaScript for NodeJS.

## Environments
Developed on `MacOS 10.13.2` with `Node.js 9.2.1`

#### Compatible/tested Environments
`MacOS 10.13.2, Node.js 8.3.0`

#### Incompatible Environments
`Node.js < 8.3.0` (this game makes use of makes use of `async/await` (Node.js > 7.6) and `console.clear` (Node.js > 8.3.0))

## Dependencies
`Node.js >= 8.3.0`
Connect Four requires [NodeJS](https://nodejs.org/en/download/) to run - all other dependencies will be installed in the [Installation](#installation) step

## Installation
To get started, `cd` into the root directory and run:

```
npm install
```

## Usage
To run the application, run the command:

```
npm start
```

By default, this will run the game with one human player (you) and one computer player.

## Testing
Testing uses [Jest](https://facebook.github.io/jest/), and can be run via:

```
npm test
```

## Overview
Connect Four is designed to prompt commands from human players, and then use those commands to direct the game. For example,
 the game may ask a Player where they want to place their piece, then show them the result of that action in ASCII.
 
 The game currently allows for one human player, and one computer player (which just selects random moves).
 
### Input 
Connect Four will only ever prompt for single worded input (for example `1`, `2` or `3`). These options will always be prompted by the game.

### Output
The output is context specific of what the user inputted. Check out the [Design](#design) section for a in-depth look at the output.

### Design
Once the game has started, the game will show an empty board:

```
Current state of the board...
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
  1   2   3   4   5   6   7

```

It will then prompt the user for the next move:

```
Columns 1, 2, 3, 4, 5, 6, 7 can be filled
Which column do you want to drop your piece in?
```

If at anytime a player inputs an invalid move, the game will let the player know:

```
Hey, you can't do that! It isn't a valid column!!!

```

If the input is valid, however, it will add the piece to the board and continue the game:
```
Current state of the board...
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
| _ | _ | _ | _ | _ | _ | _ |
| X | _ | _ | _ | _ | _ | _ |
  1   2   3   4   5   6   7

```

If a winning move is made by the player, the game will end with the message:
```
Congratulations - You've WON!!!
```

... but if a computer wins, the game will end with the message:
```
Oh no... the Computer won! Try again next time :'(
```